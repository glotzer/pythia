# Pythia

Pythia is a library to generate numerical descriptions of particle
systems.

## Documentation

See the full documentation at https://pythia-learn.readthedocs.io
. Alternatively, build it from source:

```
cd doc
make html
```
