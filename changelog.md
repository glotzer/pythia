## v0.2

- Noisy averaging of neighbor-averaged spherical harmonics (mspells)
- Bugfix: fixed width of neighbor-averaged spherical harmonics (mspells)

## v0.1

- Basic bonds, spherical_harmonic, voronoi functionality (mspells)
